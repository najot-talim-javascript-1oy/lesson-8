// 1. Quyidagi objectni outputdagi ko'rinishda chiqaring. (toString, join)

// const me = {
//     firstName: "Samariddin",
//     lastName: "Nurmamatov",
//     age: 23,
//     languages: ["js", "python", "c++", "nodejs"],
//     friends: ["Jamshid", "Abbos", "Jalol", "Mar'uf"],
// };
  
// console.log("firstName: ", me.firstName);
// console.log("lastName:", me.lastName);
// console.log("age: ", me.age);
// console.log("languages: ", me.languages.toString(''));
// console.log("friends: ", me.friends.join('+'));
 

// 2.  Kalitlari 1 dan n gacha bo’lganlar sonlarga, qiymatlari esa o’sha sonlarning kvatratiga teng object hosil qiling. (for)

// function Kv(n){
//     const kvadrat = {};
//     for(let i = 1; i <= n; i++){
//         kvadrat[i] = i * i;
//     }
//     return squares;
// }
// console.log(Kv(5));


// 3.  2-misoldan hosil bo’lgan objectning kalitlari va qiymatlari yig’indisini toping. (Object.keys(), Object.values())

// function KvQiym(obj) {
//     const values = Object.values(obj);
//     let sum = 0;
//     for (let i = 0; i < values.length; i++) {
//       sum += values[i];
//     }
//     return sum;
// }
// const obj = {1: 1, 2: 4, 3: 9, 4: 16, 5: 25,};
// const sum = KvQiym(obj);
// console.log(sum); 


// 4.  Stringlar massivi berilgan. Shunday object hosil qilingki, o’sha objectning kalitlari massiv elementlaridan va qiymatlari esa ularning uzunligidan iborat bo’lsin. (for) Input: ["Abdulaziz", "Safarmurod", "O’rol", "Jahongir"] Output: {"Abdulaziz": 9, "Safarmurod": 10, "O’rol": 5, "Jahongir": 8}

// function KvQiym(me) {
//     let obj = {};
//     for (let i = 0; i < me.length; i++) {
//         obj[me[i]] = me[i].length;
//     }
//     return obj;
// }

// let me = ["Samariddin", "Safarmurod", "O’rol", "Jahongir"]
// const obj = KvQiym(me);
// console.log(obj);



// 5 Shunday object berilganki, uning kalitlari mahsulotlardan va qiymatlari esa ularning narxlaridan tuzilgan. Barcha mahsulot qancha turishini toping. (Object.values()) Input: {"Apelsin": 10000, "Olma": 12000, "Mandarin": 8000, "Banan": 20000} Output: 50000

// function Price(fruits) {
//     var values = Object.values(fruits);
//     var sm = 0;
//     for (var i = 0; i < values.length; i++) {
//       sm += values[i];
//     }  
//     return sm;
// }
// var fruits = {"Apelsin": 10000, "Olma": 12000, "Mandarin": 8000, "Banan": 20000};
// var toPr = Price(fruits);
// console.log(toPr);


// 6.  Object kalitlarining barchasi string toifasidagi ma’lumotlardan tuzilgan bo’lsa true, aks holda false qiymat qaytaruvchi defineObject nomli funksiya hosil qiling. (Object.keys()) Input: {abc: 1, 123: 2, d: 5} Output: false


// function defineObject(object) {
//     var keys = Object.keys(object);
//     for (let i = 0; i < keys.length; i++) {
//       if (typeof object[keys[i]] !== "string") {
//         return false;
//       }
//     }
//     return true;
// }
  
// var myObject1 = {a: "Hello", b: "world"};
// console.log(defineObject(myObject1)); 

// //true
  
// var myObject2 = {a: "Hello", b: 42};
// console.log(defineObject(myObject2)); 

//false


// 7.  Gapdagi eng uzun va eng qisqa so’zlarni aniqlab {minWord: "..", maxWord: "…"} ko’rinishida qiymat qaytaruvchi minMaxWord nomli funksiya yozing. (split, for) Input: "Men Abdulaziz Programmerning guruhlarida o’qiyman." Output: {minWord: "Men", maxWord: "Programmerning"}

// function minMaxWord(text) {
//     var txts = text.split(" ");
//     var qisqatxt = txts[0];
//     var uzuntxt = txts[0];
//     for (var i = 1; i < txts.length; i++) {
//       var soz = txts[i];
//       if (soz.length < qisqatxt.length) {
//         qisqatxt = soz;
//       }
//       if (soz.length > uzuntxt.length) {
//         uzuntxt = soz;
//       }
//     }
//     return {MinWord: qisqatxt, MaxWord: uzuntxt};
// }  
// var text = "Men Abdulaziz Programmerning guruhlarida o’qiyman.";
// console.log(minMaxWord(text));


//8.  Agar object qiymatlari ichida falsy qiymatlar bo’lsa, ularni o’chirib yangi object hosil qiling. (Object.keys()) Input: {a: false, b: 12, c: true, d: 0} Output: {b: 12, c: true}

// function FalRemove(obj) {
//     var yObj = {};
//     var keys = Object.keys(obj);
//     for (var i = 0; i < keys.length; i++) {
//       var key = keys[i];
//       var value = obj[key];
//       if (value) {
//         yObj[key] = value;
//       }
//     }
//     return yObj;
//   }
  
// let obj = {a: false, b: 12, c: true, d: 0};
// let yObj = FalRemove(obj);
// console.log(yObj);
  
// 9.  Quyidagi objectlardan tuzilgan massiv ichidagi yoshi eng katta bo’lgan insonning ismini qaytaruvchi getNameMaxAge nomli funksiya yozing. (for) Input: const people = [   { name: "Abdulaziz", age: 23 }, { name: "Erkin", age: 20 }, { name: "Temur", age: 21 },]; Output: "Abdulaziz"

// function getNameMaxAge(people) {
//     var mAge = -Infinity;
//     var name = "";
//     for (var i = 0; i < people.length; i++) {
//       var person = people[i];
//       if (person.age > mAge) {
//         mAge = person.age;
//         name = person.name;
//       }
//     }
//     return name;
// }
// var people = [
//     { name: "Abdulaziz", age: 23 },
//     { name: "Erkin", age: 20 },
//     { name: "Temur", age: 21 },
// ];
// var name = getNameMaxAge(people);
// console.log(name);


// 10. Bir necha takrorlanuvchi sonlar ishtirok etgan massivdan shunday obyekt hosil qilingki, bu object kalitlari massivning takrorlanmas sonlaridan, qiymatlari esa o’sha sonlarning massivda necha marta ishtirok etganidan tuzilgan bo’lsin. (for) Input: [7, 8, 4, 5, 7, 5, 4, 8, 5, 4, 7, 9] Output: {7: 3, 8: 2, 4: 3, 5: 3, 9: 1}

// function countOccurrences(arr) {
//     var obj = {};
//     for (var i = 0; i < arr.length; i++) {
//       var num = arr[i];
//       if (obj[num]) {
//         obj[num]++;
//       } else {
//         obj[num] = 1;
//       }
//     }
//     return obj;
// }  
// var arr = [7, 8, 4, 5, 7, 5, 4, 8, 5, 4, 7, 9];
// var obj = countOccurrences(arr);
// console.log(obj);
  

// 11. Uzun sondan shunday obyekt hosil qilingki, bunda object kalitlari sinflardan, qiymatlari esa o’sha sonning mos ravishdagi o’sha sinfdagi qiymatlaridan tuzilgan bo’lsin. (for, slice) Input: 8945472985629; Output: {birlar: 629, minglar: 985, millionlar: 472, milliardlar: 945, trilionlar: 8}

// function createObj(num) {
//     const sn = String(num).split("").map(Number);
//     const obj = {};
//     obj.birlar = sn.slice(-3).join("");
//     obj.minglar = sn.slice(-6, -3).join(""); 
//     obj.millionlar = sn.slice(-9, -6).join("");
//     obj.milliardlar = sn.slice(-12, -9).join("");
//     obj.trilionlar = sn.slice(0, -12).join(""); 
//     return obj;
// }  
// console.log(createObj(8945472985629));


// 12. Quyidagi ko'rinishda objectlarlardan tuzilgan massiv beriladi. Agar alreadyRead propertysi true bo'lsa o'sha kitob o'qigan deb chiqarilsin, aks holda o'qilmagan.

// const kitoblar = [
//     {
//       title: "Halqa",
//       author: "Akrom Malik",
//       alreadyRead: false,
//     },
//     {
//       title: "Dunyo ishlari",
//       author: "O'tkir Hoshimov",
//       alreadyRead: true,
//     },
//     {
//       title: "Vaqtning qadri",
//       author: "Abdulfattoh Abu G'udda",
//       alreadyRead: false,
//     },
// ];
// for(let i = 0; i < kitoblar.length; i++){
//     const book = kitoblar[i];
//     const bookMlm = `${book.author}ning "${book.title}" kitobi `;
//     if(book.alreadyRead){
//       console.log(bookMlm + "o'qilgan;");
//     }else{
//       console.log(bookMlm + "o'qilmagan;");
//     }
// }

// 13. Objectda turli xil kalitlar bir xil qiymatlarga ega, kalitlari o'sha qiymatlardan iborat shunday object tuzingki, bu objectning qiymatlari massiv ko'rinishidagi eski objectning kalitlarlaridan iborat bo'lsin.

// function eskobj(obj) {
//     const natj = {};
//     Object.keys(obj).forEach((key) => {
//       const value = obj[key];
//       if (!natj[value]) {
//         natj[value] = [];
//       }
//       natj[value].push(Number(key));
//     });
//     return natj;
// }
// const obj = {1: 20, 2: 30, 3: 20, 4: 40, 5: 30, 6: 50, 7: 40, 8: 20};
// console.log(eskobj(obj));
  
// 14. Ikkita object berilgan ularning ba'zi kalitlari bir xil. Bir xil kalitlaridan iborat bo'lmagan yangi object hosil qiling. Input: obj1 = { a: 3, b: 10, c: 5, d: 7 }; obj2 = { a: 10, d: 4, e: 6, f: 15 }; Output: obj = {b: 10, c: 5, e: 6, f: 15}

// function ikikate(obj1, obj2) {
//     const natj = {};
//     for (const key in obj1) {
//       if (obj1.hasOwnProperty(key) && !obj2.hasOwnProperty(key)) {
//         natj[key] = obj1[key];
//       }
//     }
//     for (const key in obj2) {
//       if (obj2.hasOwnProperty(key) && !obj1.hasOwnProperty(key)) {
//         natj[key] = obj2[key];
//       }
//     }
//     return natj;
// }
// const obj1 = { a: 3, b: 10, c: 5, d: 7 };
// const obj2 = { a: 10, d: 4, e: 6, f: 15 };
// const natj = ikikate(obj1, obj2);
// console.log(natj);
  

// 15 Mahsulotlar massividagi objectlarni sonini, chegirmasini hisobga olib, umumiy summani hisoblang.

// const products = [
//     { name: "Product 1", price: 20000, discount: 10, quantity: 5 },
//     { name: "Product 2", price: 10000, discount: 20, quantity: 4 },
//     { name: "Product 3", price: 15000, discount: 8, quantity: 10 },
//     { name: "Product 4", price: 18000, discount: 5, quantity: 6 },
//     { name: "Product 5", price: 5000, discount: 10, quantity: 16 },
//   ];  
// let HamaMqd = 0;
// let totalDiscount = 0;
// let totalPrice = 0;  
// for(let i = 0; i < products.length; i++) {
//     HamaMqd += products[i].quantity;
//     totalDiscount += (products[i].price * products[i].total) / 100;
//     totalPrice += products[i].price;
// }
// console.log(`Jami mahsulotlar soni: ${HamaMqd}`);
// console.log(`Jami chegirmalar summasi: ${totalDiscount}`);
// console.log(`Umumiy summa: ${totalPrice}`);
  

// 16. 15-misoldagi mahsulotlardan, umumiy hisobda eng katta va eng kichik qiymatga ega bo'lgani toping (soni va chegirmasini ham hisobga oling)

// function calculateTotal(products) {
//     let sum = 0;
//     let maxPrice = 0;
//     let minPrice = Infinity;
//     let maxDiscount = 0;
//     let minDiscount = Infinity;
//     let maxQuantity = 0;
//     let minQuantity = Infinity;
    
//     products.forEach((product) => {
//       // sum the total price after discount for each product
//       sum += (product.price - (product.price * product.discount / 100)) * product.quantity;
  
//       // find the maximum and minimum price
//       if (product.price > maxPrice) {
//         maxPrice = product.price;
//       }
//       if (product.price < minPrice) {
//         minPrice = product.price;
//       }
  
//       // find the maximum and minimum discount
//       if (product.discount > maxDiscount) {
//         maxDiscount = product.discount;
//       }
//       if (product.discount < minDiscount) {
//         minDiscount = product.discount;
//       }
//       if (product.quantity > maxQuantity) {
//         maxQuantity = product.quantity;
//       }
//       if (product.quantity < minQuantity) {
//         minQuantity = product.quantity;
//       }
//     });
//     return {
//       totalSum: sum,
//       maxPrice: maxPrice,
//       minPrice: minPrice,
//       maxDiscount: maxDiscount,
//       minDiscount: minDiscount,
//       maxQuantity: maxQuantity,
//       minQuantity: minQuantity
//     }
// }
// const products = [
//     { name: "Product 1", price: 20000, discount: 10, quantity: 5 },
//     { name: "Product 2", price: 10000, discount: 20, quantity: 4 },
//     { name: "Product 3", price: 15000, discount: 8, quantity: 10 },
//     { name: "Product 4", price: 18000, discount: 5, quantity: 6 },
//     { name: "Product 5", price: 5000, discount: 10, quantity: 16 },
// ];  
// const result = calculateTotal(products);
// console.log(result);
  
// 17. Objectning kalitlaridan va qiymatlaridan iborat bo'lgan massiv tuzing

// const obj = {
//     "it": 20,
//     "mushuk": 10,
//     "sigir": 200,
//     "tovuq": 2
// };  
// function arrobj(obj) {
//     let arr = [];
//     for (let key in obj) {
//       arr.push(key, obj[key]);
//     }
//     return arr;
// }  
// console.log(arrobj(obj)); 
  

// 18. GPA ni hisoblovchi dastur tuzing. GPA = (grade1 * kredit1 + grade2 * kredit2 + ...) / (kredit1 + kredit2 + ...)

// function GPA(grades) {
//     let sumGr = 0;
//     let sumK = 0;
//     for (let i = 0; i < grades.length; i++) {
//       sumGr += grades[i].grade * grades[i].kredit;
//       sumK += grades[i].kredit;
//     }
//     const gpa = sumGr / sumK;
//     return gpa;
// }
// const grades = [
//     { name: "Fizika", grade: 4, kredit: 6 },
//     { name: "Matematika", grade: 5, kredit: 6 },
//     { name: "Tarix", grade: 4, kredit: 4 },	
//     { name: "Dasturlash", grade: 5, kredit: 8 },
//     { name: "Kibrxavfsizlik", grade: 4, kredit: 8 },
// ];
// const gpa = GPA(grades);
// console.log(`GPA: ${gpa}`);
  

// 19 Palonchiyev nechta to'g'ri va noto'g'ri javob topganligini toping.

// function checkAnswers(riAnswers, myAnswers) {
//     let Tugri = 0;
//     let xato = 0;
//     for (let question in riAnswers) {
//       if (riAnswers[question] === myAnswers[question]) {
//         Tugri++;
//       } else {
//         xato++;
//       }
//     }
//     return { Tugri, xato };
// }
// const riAnswers = { 1: "B", 2: "A", 3: "C", 4: "D", 5: "B", 6: "C", 7: "A", 8: 'D', 9: "A", 10: "B" };
// const myAnswers = {1: "C", 2: "A", 3: "D", 4: "D", 5: "B", 6: "C", 7: "B", 8: "C", 9: "A", 10: "C"};
// const result = checkAnswers(riAnswers, myAnswers);
// console.log(result); 
  

// 20. Qiymatlari sonlardan iborat object berilgan. Qiymatlarini n martaga oshiruvchi dastur yozing.

// function muValues(obj, n) {
//     for (let key in obj) {
//       obj[key] *= n;
//     }
//     return obj;
// }
// const obj = { a: 2, b: 3, c: 4, d: 6 };
// const n = 3;
// const res = muValues(obj, n);
// console.log(res); 
    

// 21. Quyidagi obyektdan destructing orqali barcha qiymatlarini oling.

// const {
//     name: productName,
//     company: {
//       name: companyName,
//       price: productPrice,
//       founder: {
//         firstName: founderFirstName,
//         lastName: founderLastName,
//         birthDate: founderBirthDate,
//       },
//     },
//   } = product

// productName = "Iphone 14"
// companyName = "Apple"
// productPrice = "200 mlrd"
// founderFirstName = "Steve"
// founderLastName = "Jobs"
// founderBirthDate = 1950


// 22. Quyidagi massivdagi barcha o'quvchilarni protcentlarining o'rtacha qiymatini toping, shuningdek, objectlarga quyidagi propertylarni qo'shib yangi massiv qaytaring.

// const students = [
//     { name: "Samariddin", percentage: 80 },
//     { name: "Nematullo", percentage: 90 },
//     { name: "Husniddin", percentage: 95 },
//     { name: "Oktyabr", percentage: 70 },
//   ];
//   const totalPercentage = students.reduce((acc, student) => acc + student.percentage, 0) / students.length;
//   console.log(totalPercentage);
//   const newStudents = students.map(student => {
//     return {
//       name: student.name,
//       percentage: student.percentage,
//       isPassing: student.percentage >= 60 ? true : false
//     }
//   });
  
//   console.log(newStudents);

  
// 23. grade propertyga protcent 90-100 o'rtasida bo'lsa 5, 80-90 o'rtasida bo'lsa 4, 70-80 o'rtasida bo'lsa 3 bahoni, qolgan holatlarda 2 bahoni o'zlashtiring.

// function getGrade(percentage) {
//     if (percentage >= 90 && percentage <= 100) {
//       return 5;
//     } else if (percentage >= 80 && percentage < 90) {
//       return 4;
//     } else if (percentage >= 70 && percentage < 80) {
//       return 3;
//     } else {
//       return 2;
//     }
//   }
//   console.log(getGrade(85)); // 4
//   console.log(getGrade(63)); // 2
    


// 24. isPassed propertyga protcent 70 dan o'tsa va teng bo’lsa true, aks holda false qiymat o'zlashtirilsin.

// function isPassed(percentage) {
//     if (percentage >= 70) {
//       return true;
//     } else {
//       return false;
//     }
// }
// console.log(isPassed(80)); // true
// console.log(isPassed(65)); // false
// console.log(isPassed(70)); // true
    
  
  
// 25  Necha kishi imtihondan o'tdi va necha kishi imtihonda o'ta olmadi shuni ham hisoblang. const pupils

// const pupils = [
//     {
//       name: "Samariddin",
//       percent: 95,
//     },
//     {
//       name: "Muslihiddin",
//       percent: 78,
//     },
//     {
//       name: "Laziz",
//       percent: 83,
//     },
//     {
//       name: "Nematullo",
//       percent: 88,
//     },
//     {
//       name: "Ali",
//       percent: 66,
//     },
//     {
//       name: "Kamron",
//       percent: 75,
//     },
// ];  
// let passedCount = 0;
// let failedCount = 0;
// for (let i = 0; i < pupils.length; i++) {
//     if (pupils[i].percent >= 70) {
//       passedCount++;
//     } else {
//       failedCount++;
//     }
// }
// console.log(`Imtihondan o'tkan o'quvchilar soni: ${passedCount}`);
// console.log(`Imtihonda o'ta olmaygan o'quvchilar soni: ${failedCount}`);
  